package com.quizapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.quizapp.services.UserService;

import javax.inject.Inject;

import static com.quizapp.util.Constant.SPLASH_TIME_OUT;

public class SplashActivity extends AppCompatActivity {


    @Inject
    protected UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        QuizApplication.getComponent(getApplication()).inject(this);

        new Handler().postDelayed(() -> {
            Intent i = new Intent(SplashActivity.this, userService.isLogged() ? MainActivity.class : LoginActivity.class);
            startActivity(i);
            finish();
        }, SPLASH_TIME_OUT);
    }
}
