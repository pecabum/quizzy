package com.quizapp;

import android.app.Application;
import android.content.Context;

import com.quizapp.di.AppComponent;
import com.quizapp.di.AppModule;
import com.quizapp.di.DaggerAppComponent;


/**
 * The packages are constructed that way because of the small project.
 * I'm grouping the classes and sub-packages by context when the project is bigger
 */
public class QuizApplication extends Application {

    private AppComponent appComponent;

    public static AppComponent getComponent(Context context) {
        return ((QuizApplication) context.getApplicationContext()).appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

}
