package com.quizapp.interfaces;

public interface OnCompletedDialogListener {
    void onStartAgainClicked();
}
