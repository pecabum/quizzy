package com.quizapp.interfaces;

import android.view.View;

import com.quizapp.model.dao.Answer;

/**
 * Created by petar on 07.06.17.
 */

public interface OnAnswerListener {
    void onMultiChoiceAnswerClicked(View view, Answer correctAnswer);

    void onBinaryChoiceSelected(Answer answer, boolean choice,Answer correctAnswer);
}
