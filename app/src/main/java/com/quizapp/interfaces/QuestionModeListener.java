package com.quizapp.interfaces;

/**
 * Created by petar on 07.06.17.
 */

public interface QuestionModeListener {
    void onQuestionModeChanged(boolean modeMultiple);
}
