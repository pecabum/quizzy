package com.quizapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.quizapp.QuizApplication;
import com.quizapp.model.AppDatabase;
import com.quizapp.model.dao.Answer;
import com.quizapp.model.dao.Question;
import com.quizapp.services.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class QuestionsViewModel extends AndroidViewModel {

    @Inject
    protected AppDatabase database;

    @Inject
    protected UserService userService;

    private LiveData<List<Question>> questions;

    private ArrayList<Answer> answers = new ArrayList<>();

    public QuestionsViewModel(Application application) {
        super(application);
        QuizApplication.getComponent(application).inject(this);
    }

    public void subscribeToChanges() {
        questions = database.questionDao().loadQuestions();
    }

    public boolean getMultipleChoice() {
        return userService.isMultipleChoice();
    }

    public LiveData<List<Question>> getQuestions() {
        return questions;
    }

    public Question getCurrentQuestion() {
        return questions.getValue().get(answers.size());
    }

    public void putAnswer(Answer answer) {
        answers.add(answer);
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void clearStats() {
        answers.clear();
    }
}
