package com.quizapp.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.quizapp.util.Constant;
import com.quizapp.fragments.SettingsFragment;
import com.quizapp.fragments.ProfileFragment;
import com.quizapp.fragments.QuizFragment;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);

        mFragmentList.add(QuizFragment.newInstance());

        // Placing Quiz fragment as target fragment to be able to subscribe for changes
        SettingsFragment settingsFragment = SettingsFragment.newInstance();
        settingsFragment.setTargetFragment(mFragmentList.get(0), Constant.REQUEST_CODE_MODE_CHANGE);

        mFragmentList.add(settingsFragment);
        mFragmentList.add(ProfileFragment.newInstance());
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

}