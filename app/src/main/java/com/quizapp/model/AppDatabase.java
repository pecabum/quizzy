package com.quizapp.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.quizapp.model.dao.Answer;
import com.quizapp.model.dao.AnswerDao;
import com.quizapp.model.dao.QuestionDao;
import com.quizapp.model.dao.Quote;
import com.quizapp.model.dao.QuoteDao;

@Database(entities = {Quote.class, Answer.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "questions-db";

    public abstract QuoteDao quoteDao();

    public abstract AnswerDao answerDao();

    public abstract QuestionDao questionDao();
}
