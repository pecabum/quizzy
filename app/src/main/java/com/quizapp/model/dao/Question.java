package com.quizapp.model.dao;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

public class Question {

    @Embedded
    private Quote quote;

    @Relation(parentColumn = "id", entityColumn = "quote_id")
    private List<Answer> answers;

    public Quote getQuote() {
        return quote;
    }

    public void setQuote(Quote quote) {
        this.quote = quote;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List answers) {
        this.answers = answers;
    }
}
