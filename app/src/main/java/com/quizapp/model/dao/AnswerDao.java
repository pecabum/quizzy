package com.quizapp.model.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface AnswerDao {

    @Query("SELECT * FROM answers")
    LiveData<List<Answer>> getAllAnswers();

    @Query("SELECT * FROM answers WHERE quote_id = :quoteId")
    List<Answer> getAllAnswersByQuestionId(int quoteId);

    @Insert(onConflict = REPLACE)
    void insertAnswer(Answer question);

    @Insert(onConflict = IGNORE)
    void insertAllAnswers(Answer... answers);
}
