package com.quizapp.model.dao;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity
        (tableName = "answers",
                foreignKeys = @ForeignKey(entity = Quote.class,
                        parentColumns = "id",
                        childColumns = "quote_id",
                        onDelete = CASCADE
                ))
public class Answer implements Parcelable{

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "quote_id")
    public long quoteId;

    private String answer;

    private boolean correct;

    public Answer(long quoteId, String answer, boolean correct) {
        this.quoteId = quoteId;
        this.answer = answer;
        this.correct = correct;
    }

    protected Answer(Parcel in) {
        id = in.readInt();
        quoteId = in.readLong();
        answer = in.readString();
        correct = in.readByte() != 0;
    }

    public static final Creator<Answer> CREATOR = new Creator<Answer>() {
        @Override
        public Answer createFromParcel(Parcel in) {
            return new Answer(in);
        }

        @Override
        public Answer[] newArray(int size) {
            return new Answer[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getQuestionId() {
        return quoteId;
    }

    public void setQuestionId(long quoteId) {
        this.quoteId = quoteId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeLong(quoteId);
        dest.writeString(answer);
        dest.writeByte((byte) (correct ? 1 : 0));
    }
}

