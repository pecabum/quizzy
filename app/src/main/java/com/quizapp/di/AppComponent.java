package com.quizapp.di;

import com.quizapp.LoginActivity;
import com.quizapp.MainActivity;
import com.quizapp.SplashActivity;
import com.quizapp.fragments.ProfileFragment;
import com.quizapp.fragments.SettingsFragment;
import com.quizapp.viewmodel.QuestionsViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(SplashActivity splashActivity);

    void inject(LoginActivity loginActivity);

    void inject(MainActivity mainActivity);

    void inject(ProfileFragment profileFragment);

    void inject(SettingsFragment settingsFragment);

    void inject(QuestionsViewModel questionsViewModel);

}