package com.quizapp.di;

import android.arch.persistence.room.Room;
import android.content.Context;


import com.quizapp.QuizApplication;
import com.quizapp.model.AppDatabase;
import com.quizapp.services.UserService;
import com.quizapp.services.UserServiceImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final QuizApplication app;

    public AppModule(QuizApplication app) {
        this.app = app;
    }

    @Provides
    Context provideApplicationContext() {
        return app.getApplicationContext();
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, AppDatabase.DATABASE_NAME).build();
    }

    @Provides
    @Singleton
    UserService provideUserService(Context context) {
        return new UserServiceImpl(context);
    }

}