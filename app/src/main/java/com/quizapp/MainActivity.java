package com.quizapp;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.quizapp.adapters.ViewPagerAdapter;
import com.quizapp.databinding.ActivityMainBinding;
import com.quizapp.services.UserService;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    protected UserService userService;

    private MenuItem prevMenuItem;
    private ActivityMainBinding binding;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener =
            item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        binding.viewpager.setCurrentItem(0, false);
                        break;
                    case R.id.navigation_notifications:
                        binding.viewpager.setCurrentItem(1, false);
                        break;
                    case R.id.navigation_dashboard:
                        binding.viewpager.setCurrentItem(2, false);
                        break;

                }
                return true;
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            QuizApplication.getComponent(getApplication()).inject(this);
            binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
            setupUI();
    }

    private void setupUI() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        binding.viewpager.setOffscreenPageLimit(2);
        binding.viewpager.setAdapter(adapter);
        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    binding.navigation.getMenu().getItem(0).setChecked(false);
                }
                prevMenuItem = binding.navigation.getMenu().getItem(position);
                prevMenuItem.setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        binding.navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
