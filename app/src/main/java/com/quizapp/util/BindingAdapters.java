package com.quizapp.util;


import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.quizapp.model.dao.Answer;

public class BindingAdapters {

    @BindingAdapter({"visibility"})
    public static void setVisibility(final View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter({"bind:error"})
    public static void setError(TextView textView, String date) {
        if (date != null) {
            textView.setText(date);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.INVISIBLE);
        }
    }

    @BindingAdapter({"app:answer"})
    public static void setAnswer(Button textView, Answer answer) {
        if (answer != null) {
            textView.setText(answer.getAnswer());
            textView.setTag(answer);
        }
    }

    @BindingAdapter({"app:answer"})
    public static void setAnswer(TextView textView, Answer answer) {
        if (answer != null) {
            textView.setText(answer.getAnswer());
            textView.setTag(answer);
        }
    }
}
