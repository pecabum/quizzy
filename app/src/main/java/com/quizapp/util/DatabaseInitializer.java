package com.quizapp.util;

import android.os.AsyncTask;

import com.quizapp.model.AppDatabase;
import com.quizapp.model.dao.Answer;
import com.quizapp.model.dao.Quote;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DatabaseInitializer {

    public static void populateAsync(final AppDatabase db) {
        PopulateDbAsync task = new PopulateDbAsync(db);
        task.execute();
    }

    private static void populateWithTestData(AppDatabase db) {
        List<Quote> quotes = db.quoteDao().getAll();
        if (quotes.size() == 0) {
            generateDb(db);
        }
    }

    private static void generateDb(AppDatabase db) {
        Random ran = new Random();
        List<Answer> answers;

        for (int i = 0; i < 75; i++) {
            Quote quote = new Quote();
            quote.setQuote("Quote " + i);
            answers = new ArrayList<>();

            long quoteId = db.quoteDao().insertQuote(quote);
            int correctAnswerPosition = ran.nextInt(3);

            for (int j = 0; j < 3; j++) {
                answers.add(new Answer(quoteId, "Answer " + j, j == correctAnswerPosition));
            }

            db.answerDao().insertAllAnswers(answers.toArray(new Answer[answers.size()]));
        }
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;

        PopulateDbAsync(AppDatabase db) {
            mDb = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateWithTestData(mDb);
            return null;
        }

    }
}
