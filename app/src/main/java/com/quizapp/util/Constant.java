package com.quizapp.util;


public class Constant {
    public static final String KEY_SELECTED_ANSWER = "selected_answer";
    public static final String KEY_CORRECT_ANSWER = "correct_answer";
    public static final String KEY_CHOICE = "choice";


    public static final int ANSWERS_SIZE_COMPLETED = 10;
    public static final int REQUEST_CODE_MODE_CHANGE = 101;
    public static final int REQUEST_CODE_COMPLETE_QUIZ = 332;
    public static final int REQUEST_CODE_ANSWER = 1000;
    public static final int SPLASH_TIME_OUT = 2000;

    public static final String KEY_ANSWERS = "answers";
}
