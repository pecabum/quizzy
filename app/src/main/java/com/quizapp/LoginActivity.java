package com.quizapp;

import android.arch.lifecycle.LifecycleActivity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.inputmethod.EditorInfo;

import com.quizapp.databinding.ActivityLoginBinding;
import com.quizapp.model.AppDatabase;
import com.quizapp.services.UserService;
import com.quizapp.util.DatabaseInitializer;

import javax.inject.Inject;


public class LoginActivity extends LifecycleActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    @Inject
    protected UserService userService;

    @Inject
    protected AppDatabase appDatabase;

    private ActivityLoginBinding binding;

    private ObservableField<String> emailError = new ObservableField<>();
    private ObservableField<String> passError = new ObservableField<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        QuizApplication.getComponent(getApplication()).inject(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        setupUI();
        DatabaseInitializer.populateAsync(appDatabase);
    }

    private void setupUI() {
        binding.etPassword.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        });

        binding.btnSignIn.setOnClickListener(view -> attemptLogin());

        binding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passError.set(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailError.set(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.setEmailError(emailError);
        binding.setPassError(passError);
    }

    private void attemptLogin() {
        // Reset errors.
        emailError.set(null);
        passError.set(null);

        // Store values at the time of the login attempt.
        String email = binding.etEmail.getText().toString();
        String password = binding.etPassword.getText().toString();

        boolean cancel = false;

        if (TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailError.set(getString(R.string.error_field_required));
            cancel = true;
        } else if (!isEmailValid(email)) {
            emailError.set(getString(R.string.error_invalid_email));
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            passError.set(getString(R.string.error_field_required));
            cancel = true;
        } else if (!isPasswordValid(password)) {
            passError.set(getString(R.string.error_invalid_password));
            cancel = true;
        }

        if (!cancel) {
            userService.login(email, password);
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
    }

    private boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

}

