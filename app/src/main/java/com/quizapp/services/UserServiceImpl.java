package com.quizapp.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.quizapp.services.UserService;

import javax.inject.Singleton;

/**
 * Ugly realisation but enough for this deadline
 */
@Singleton
public class UserServiceImpl implements UserService {

    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_USER_PASS = "user_password";
    private static final String KEY_ADMIN_PREFS = "admin_prefs";
    private static final String KEY_MULTIPLE = "multiple_choice";
    private static final String KEY_LOGGED = "user_logged";

    private SharedPreferences preferences;

    public UserServiceImpl(Context context) {
        preferences = context.getSharedPreferences(KEY_ADMIN_PREFS, Context.MODE_PRIVATE);
    }

    @Override
    public String getUserEmail() {
        return preferences.getString(KEY_USER_EMAIL, "");
    }

    @Override
    public String getUserPassword() {
        return preferences.getString(KEY_USER_PASS, "");
    }

    @Override
    public boolean isLogged() {
        return preferences.getBoolean(KEY_LOGGED, false);
    }

    @Override
    public boolean isMultipleChoice() {
        return preferences.getBoolean(KEY_MULTIPLE, false);
    }

    @Override
    public void setMultipleChoice(boolean multipleChoice) {
        preferences.edit().putBoolean(KEY_MULTIPLE, multipleChoice).apply();
    }

    @Override
    public void login(String email, String password) {
        preferences.edit().putString(KEY_USER_EMAIL, email).apply();
        preferences.edit().putString(KEY_USER_PASS, password).apply();
        preferences.edit().putBoolean(KEY_LOGGED, true).apply();
    }

    @Override
    public void logout() {
        preferences.edit().clear().apply();
    }


}
