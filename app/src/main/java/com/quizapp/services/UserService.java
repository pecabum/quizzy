package com.quizapp.services;

public interface UserService {
    String getUserEmail();

    String getUserPassword();

    boolean isLogged();

    boolean isMultipleChoice();

    void setMultipleChoice(boolean multipleChoice);

    void login(String email, String password);

    void logout();
}
