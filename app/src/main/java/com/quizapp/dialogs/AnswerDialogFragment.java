package com.quizapp.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.quizapp.R;
import com.quizapp.interfaces.OnAnswerDialogListener;
import com.quizapp.model.dao.Answer;
import com.quizapp.util.Constant;

public class AnswerDialogFragment extends DialogFragment {


    private Answer selectedAnswer;
    private Answer correctAnswer;

    public static AnswerDialogFragment newInstance(Fragment quizFragment, Answer answer, Answer correctAnswer) {
        AnswerDialogFragment dialogFragment = new AnswerDialogFragment();
        dialogFragment.setTargetFragment(quizFragment, Constant.REQUEST_CODE_ANSWER);

        Bundle args = new Bundle();
        args.putParcelable(Constant.KEY_SELECTED_ANSWER, answer);
        args.putParcelable(Constant.KEY_CORRECT_ANSWER, correctAnswer);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }


    public static AnswerDialogFragment newInstance(Fragment quizFragment, Answer answer, Answer correctAnswer, boolean choice) {
        AnswerDialogFragment answerDialogFragment = newInstance(quizFragment, answer, correctAnswer);
        answerDialogFragment.getArguments().putBoolean(Constant.KEY_CHOICE, choice);
        return answerDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedAnswer = getArguments().getParcelable(Constant.KEY_SELECTED_ANSWER);
        correctAnswer = getArguments().getParcelable(Constant.KEY_CORRECT_ANSWER);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (getArguments().containsKey(Constant.KEY_CHOICE)) {
            boolean choice = getArguments().getBoolean(Constant.KEY_CHOICE);
            builder.setTitle(selectedAnswer.isCorrect() == choice ? getString(R.string.correct) : getString(R.string.wrong));
        } else {
            builder.setTitle(selectedAnswer.isCorrect() ? getString(R.string.correct) : getString(R.string.wrong));
        }

        builder.setMessage(getString(R.string.right_answer, correctAnswer.getAnswer()));

        builder.setPositiveButton("Ok", (dialog, which) -> {
            returnResult();
            dismiss();
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        return alertDialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        returnResult();
    }

    private void returnResult() {
        OnAnswerDialogListener answerDialogListener = getAnswerDialogListener();

        if (answerDialogListener != null) {
            if (getArguments().containsKey(Constant.KEY_CHOICE)) {
                boolean choice = getArguments().getBoolean(Constant.KEY_CHOICE);
                selectedAnswer.setCorrect(selectedAnswer.isCorrect() == choice);
            }
            answerDialogListener.onMultipleChoiceAnswerAccepted(selectedAnswer);
        }
    }

    private OnAnswerDialogListener getAnswerDialogListener() {
        Fragment targetFragment = getTargetFragment();

        if (targetFragment instanceof OnAnswerDialogListener) {
            return (OnAnswerDialogListener) targetFragment;
        }

        return null;
    }
}