package com.quizapp.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.annimon.stream.Stream;
import com.quizapp.R;
import com.quizapp.databinding.FragmentDialogCompleteBinding;
import com.quizapp.interfaces.OnCompletedDialogListener;
import com.quizapp.model.dao.Answer;
import com.quizapp.util.Constant;

import java.util.ArrayList;

public class QuizCompletedDialogFragment extends DialogFragment {

    private FragmentDialogCompleteBinding binding;


    public static QuizCompletedDialogFragment newInstance(Fragment fragment, ArrayList<Answer> answers) {
        QuizCompletedDialogFragment dialogFragment = new QuizCompletedDialogFragment();
        dialogFragment.setTargetFragment(fragment, Constant.REQUEST_CODE_COMPLETE_QUIZ);

        Bundle args = new Bundle();
        args.putParcelableArrayList(Constant.KEY_ANSWERS, answers);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentDialogCompleteBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setMessage(getMessage());
        binding.btnStartAgain.setOnClickListener(v -> startNewQuiz());
    }

    private void startNewQuiz() {
        OnCompletedDialogListener completedDialogListener = getCompletedDialogListener();

        if (completedDialogListener != null) {
            completedDialogListener.onStartAgainClicked();
        }

        dismiss();
    }

    private String getMessage() {
        if (getArguments() != null && getArguments().containsKey(Constant.KEY_ANSWERS)) {
            ArrayList<Answer> answers = getArguments().getParcelableArrayList(Constant.KEY_ANSWERS);

            int correctAnswers = (int) Stream.of(answers).filter(Answer::isCorrect).count();
            int incorrectAnswers = answers.size() - correctAnswers;
            return getString(R.string.answers_result, correctAnswers, incorrectAnswers);
        }
        return "";
    }

    @Override
    public void onStart() {
        super.onStart();
        setupFullscreenDialog();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        startNewQuiz();
    }

    private void setupFullscreenDialog() {
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private OnCompletedDialogListener getCompletedDialogListener() {
        Fragment targetFragment = getTargetFragment();

        if (targetFragment instanceof OnCompletedDialogListener) {
            return (OnCompletedDialogListener) targetFragment;
        }

        return null;
    }

}