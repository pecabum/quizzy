package com.quizapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quizapp.LoginActivity;
import com.quizapp.QuizApplication;
import com.quizapp.databinding.FragmentProfileBinding;
import com.quizapp.services.UserService;

import javax.inject.Inject;

public class ProfileFragment extends Fragment {

    @Inject
    protected UserService userService;

    private FragmentProfileBinding binding;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProfileBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        QuizApplication.getComponent(getActivity()).inject(this);
        binding.setEmail(userService.getUserEmail());
        binding.setPassword(userService.getUserPassword());

        binding.btnLogout.setOnClickListener(v -> {
            userService.logout();
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
        });

    }
}