package com.quizapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quizapp.QuizApplication;
import com.quizapp.databinding.FragmentSettingsBinding;
import com.quizapp.interfaces.OnAnswerDialogListener;
import com.quizapp.interfaces.QuestionModeListener;
import com.quizapp.services.UserService;

import javax.inject.Inject;

public class SettingsFragment extends Fragment {

    public static final String TAG = SettingsFragment.class.getSimpleName();

    @Inject
    protected UserService userService;

    private FragmentSettingsBinding binding;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSettingsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        QuizApplication.getComponent(getActivity()).inject(this);

        binding.swMultipleChoice.setChecked(userService.isMultipleChoice());
        binding.swMultipleChoice.setOnCheckedChangeListener((buttonView, isChecked) -> {
            userService.setMultipleChoice(isChecked);
            QuestionModeListener questionModeListener = getQuestionModeListener();
            if (questionModeListener != null) {
                questionModeListener.onQuestionModeChanged(isChecked);
            }
        });
    }

    private QuestionModeListener getQuestionModeListener() {
        Fragment targetFragment = getTargetFragment();

        if (targetFragment instanceof QuestionModeListener) {
            return (QuestionModeListener) targetFragment;
        }

        return null;
    }
}