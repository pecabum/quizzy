package com.quizapp.fragments;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.Stream;
import com.quizapp.dialogs.AnswerDialogFragment;
import com.quizapp.dialogs.QuizCompletedDialogFragment;
import com.quizapp.interfaces.OnAnswerDialogListener;
import com.quizapp.interfaces.OnAnswerListener;
import com.quizapp.databinding.FragmentQuizBinding;
import com.quizapp.interfaces.OnCompletedDialogListener;
import com.quizapp.interfaces.QuestionModeListener;
import com.quizapp.model.dao.Answer;
import com.quizapp.model.dao.Question;
import com.quizapp.util.Constant;
import com.quizapp.viewmodel.QuestionsViewModel;

public class QuizFragment extends LifecycleFragment implements OnAnswerListener, OnAnswerDialogListener, QuestionModeListener, OnCompletedDialogListener {

    private FragmentQuizBinding binding;
    private QuestionsViewModel viewModel;

    public static QuizFragment newInstance() {
        return new QuizFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentQuizBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(QuestionsViewModel.class);
        fetchData();
    }

    private void fetchData() {
        viewModel.subscribeToChanges();
        viewModel.getQuestions().observe(this, questions -> {
            binding.setMultipleChoice(viewModel.getMultipleChoice());
            nextQuestion();
        });
    }

    private void nextQuestion() {
        Question question = viewModel.getCurrentQuestion();
        binding.setQuestion(viewModel.getCurrentQuestion());
        binding.setListener(this);
        Stream.of(question.getAnswers()).filter(Answer::isCorrect).findFirst().ifPresent(answer -> binding.setCorrectAnswer(answer));
    }

    @Override
    public void onMultiChoiceAnswerClicked(View view, Answer correctAnswer) {
        Answer answer = (Answer) view.getTag();
        showAnswerDialog(answer, correctAnswer);
    }

    @Override
    public void onBinaryChoiceSelected(Answer answer, boolean choice, Answer correctAnswer) {
        AnswerDialogFragment answerDialogFragment = AnswerDialogFragment.newInstance(this, answer, correctAnswer, choice);
        answerDialogFragment.show(getActivity().getSupportFragmentManager(), "");
    }

    private void showAnswerDialog(Answer answer, Answer correctAnswer) {
        AnswerDialogFragment answerDialogFragment = AnswerDialogFragment.newInstance(this, answer, correctAnswer);
        answerDialogFragment.show(getActivity().getSupportFragmentManager(), "");
    }

    @Override
    public void onMultipleChoiceAnswerAccepted(Answer answer) {
        viewModel.putAnswer(answer);
        if (viewModel.getAnswers().size() < Constant.ANSWERS_SIZE_COMPLETED) {
            nextQuestion();
        } else {
            QuizCompletedDialogFragment answerDialogFragment = QuizCompletedDialogFragment.newInstance(this, viewModel.getAnswers());
            answerDialogFragment.show(getActivity().getSupportFragmentManager(), "");
        }
    }

    @Override
    public void onQuestionModeChanged(boolean modeMultiple) {
        binding.setMultipleChoice(modeMultiple);
        viewModel.clearStats();
        fetchData();
    }

    @Override
    public void onStartAgainClicked() {
        viewModel.clearStats();
        fetchData();
    }
}