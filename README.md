Quizzy 

За генерирането на данни не съм използвал реални, понеже нямах време. Базата данни е [Room](https://developer.android.com/topic/libraries/architecture/room.html) , използвам [LiveData](https://developer.android.com/topic/libraries/architecture/livedata.html) за събиране на данните.
Използвам [DataBinding](https://developer.android.com/topic/libraries/data-binding/index.html) тъй като предоставя readability, премахва много boilerplate и опростява логиката до голяма степен(като цяло съм фен на MVVM, въпреки че не съм го имплементирал тук), както и [Retrolambda](https://github.com/evant/gradle-retrolambda). Използвам и [StreamApi](https://github.com/aNNiMON/Lightweight-Stream-API) - за бенефитите от него трябва да се прочете, много са.

**LoginActivity**

Стандартно LoginActivity , разликата е, че съм използвал Observables при валидацията , за да мога да показвам и скрива error полетата.
За запазване на данните съм използвал [SharedPrefetences](https://developer.android.com/reference/android/content/SharedPreferences.html) като те минават през Injected Service със скрита имплементация. Принципно групирам packages по контекста, в който се използват, но този application е малък и не се налага.


**MainActivity**
Използвам [viewpager](https://developer.android.com/reference/android/support/v4/view/ViewPager.html) със [BottomNavigationView](https://developer.android.com/reference/android/support/design/widget/BottomNavigationView.html) с цел да запазя инстанцията на фрагментите активна.
Можех да го направя и с отделен мениджър на фрагментите, но нямах време да го напиша.
Екранът използва 3 фрагмента.При създаването им слагам 1вия да е таргет на 2рия с цел той да има достъп до промени в поведението, можех да го реализирам с observable, но нямах време. При binary съм сложил цветовете на бутоните, но не съм правил selectors, понеже нямах време.

**QuizFragment**
Фрагментът използвa [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel.html) за съхраняване на данните.
Има 2 режима - binary & multiple choice, които се навигират изцяло от xml частта.

**SettingsFragment**
Съдържа само [Switch](https://developer.android.com/reference/android/widget/Switch.html), който да сменя режима binary <-> multiple. Състоянитето се пази в UserService. При промяна се извиква refresh на QuizFragment.

**ProfileFragment**
Съдържа информацията, въведена при логин, както и Logout бутон, който изчиства данните от UserService и отвежда потребителя до LoginActivity.